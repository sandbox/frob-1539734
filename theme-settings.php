<?php
/**
 * Implements hook_form_system_theme_settings_alter() function.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 */
function defaulty_form_system_theme_settings_alter(&$form, $form_state, $form_id = NULL) {
  // Work-around for a core bug affecting admin themes. See issue #943212.
  // taken from Zen theme
  if (isset($form_id)) {
    return;
  }

  $form['user_form_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('User Form Settings'),
    '#description' => t('These settings affect the User Login, Register, and Forgot Password Forms.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#access' => user_access('administer theme-settings'),
  );
  
  $form['user_form_settings']['defaulty_user_form_tabs_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('User Form Tabs'),
    '#description' => 'Enable or Disable the Tabs on User Register/Login Page.<br /> Checked means the Tabs will show.',
    '#default_value' => theme_get_setting('defaulty_user_form_tabs_enable'),
  );
  
  $form['user_form_settings']['registration_page_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Registration Page Settings'),
    '#description' => t('These settings effect the user Registration Page.'),
  );
  
  $form['user_form_settings']['login_page_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Login Page Settings'),
    '#description' => t('These settings effect the user Login Page.'),
    '#access' => user_access('administer theme-settings'),
  );
  
  $form['user_form_settings']['registration_page_settings']['defaulty_user_form_register_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('User Register Page Title'),
    '#description' => t('This title appears on the /user/register page'),
    '#size' => 40,
    '#maxlength' => 255,
    '#default_value' => theme_get_setting('defaulty_user_form_register_page_title'),
  );
  
  $form['user_form_settings']['registration_page_settings']['defaulty_user_form_registration_copy'] = array(
    '#type' => 'textarea',
    '#title' => t('User Register Form Copy'),
    '#description' => t('The text you enter here appears on the User Registration Page.'),
    '#default_value' => theme_get_setting('defaulty_user_form_registration_copy'),
    '#cols' => 60,
    '#rows' => 5,
  );
  
  $form['user_form_settings']['registration_page_settings']['defaulty_user_form_login_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Call to Action button for logging into an existing account'),
    '#description' => 'When checked a Call to Action button will appear on the User Registration Page directing users to Login to their existing account.',
    '#default_value' => theme_get_setting('defaulty_user_form_tabs_enable'),
  );
  
  $form['user_form_settings']['login_page_settings']['defaulty_user_form_login_page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('User Login Page Title'),
    '#description' => t('This title appears on the /user/login page'),
    '#default_value' => theme_get_setting('defaulty_user_form_login_page_title'),
    '#size' => 40,
    '#maxlength' => 255,
  );
  
  $form['user_form_settings']['login_page_settings']['defaulty_user_form_login_copy'] = array(
    '#type' => 'textarea',
    '#title' => t('User Login Form Copy'),
    '#description' => t('The text you enter here appears on the User Login page.'),
    '#default_value' => theme_get_setting('defaulty_user_form_login_copy'),
    '#cols' => 60,
    '#rows' => 5,
  );
  
  $form['user_form_settings']['login_page_settings']['defaulty_user_form_register_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display Call to Action button for registering a new account'),
    '#description' => 'When checked a Call to Action button will appear on the Login Page directing users to Register a new account.',
    '#default_value' => theme_get_setting('defaulty_user_form_tabs_enable'),
  );
}