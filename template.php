<?php
function defaulty_status_messages(&$vars) {
  //firep($vars);
  //dpm($vars);
  //dpm(arg(), 'page');
  $display = $vars['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'), 
    'error' => t('Error message'), 
    'warning' => t('Warning message'),
  );
  foreach (drupal_get_messages($display) as $type => $messages) {
    $output .= "<div class=\"message-wrapper $type\"><div class=\"messages $type\">\n";
    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
    }
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= $messages[0];
    }
    $output .= "</div>\n</div>";
  }
  return $output;
}

function defaulty_preprocess_page(&$vars) {
  /* USER
  Remove the tabs from the user login, register & password
  Also fixes page title
  */

  //firep(current_path());
  //firep($vars['tabs']['#primary'][2]['#link']['path']);
  //drupal_set_message("This is a test message.");
  //drupal_set_message("This is a test message.", 'status');
  //drupal_set_message("This is a test message.", 'warning');
  //drupal_set_message("This is a test message.", 'error');

  if(is_array($vars['tabs']['#primary'])) {
    $key = -1;
    foreach($vars['tabs']['#primary'] as $key => $tab) {
      if($tab['#link']['path'] == 'user/password') {
        $forgot_password_link_index = $key;
      }
    }
  }

  $enable_tabs = theme_get_setting('defaulty_user_form_tabs_enable');
  $user_page = FALSE;
  switch(current_path()) {
    case 'user/login':
    case 'user':
      $user_page = TRUE;
      $vars['title'] = t('Login');
      if(isset($vars['tabs']['#primary'][$key])) {
        unset($vars['tabs']['#primary'][$key]);
      }
    break;
    case 'user/register':
      $user_page = TRUE;
      $vars['title'] = t('Register New Account');
      if(isset($vars['tabs']['#primary'][$key])) {
        unset($vars['tabs']['#primary'][$key]);
      }
    break;
    case 'user/password':
      $user_page = TRUE;
      $vars['title'] = t('Forgot Your Password?');
      if(isset($vars['tabs']['#primary'][$key])) {
        unset($vars['tabs']['#primary'][$key]);
      }
    break;
  }
  if($user_page && !$enable_tabs) {
    foreach($vars['tabs']['#primary'] as $key => $tab) {
      unset($vars['tabs']['#primary'][$key]);
    }
  }
}

function defaulty_page_alter(&$vars) {
  $path = arg();
  $page = 'node';
  
  // see if the current page is the user register page or the user login page.
  if($path[0] == 'user' && $path[1] == 'register') {
    $page = 'register';
    
    $register_title = theme_get_setting('defaulty_user_form_register_page_title');
    $register_copy = theme_get_setting('defaulty_user_form_registration_copy');// . 
    if(theme_get_setting('defaulty_user_form_login_enable')) {
      $register_cta = l(t('Sign Into Your Account!'), 'user/login', array('attributes' => array('class' => 'login-register button big', 'title' => t('Create New User Account'))));;
    }
    else {
      $register_cta = '';
    }
  }
  else if($path[0] == 'user' && ($path[1] != 'register' && !is_numeric($path[1]) && $path[1] != 'password')) {
    $page = 'login';
    
    $login_title = theme_get_setting('defaulty_user_form_login_page_title');
    $login_copy = theme_get_setting('defaulty_user_form_login_copy');
    // $login_copy .= l(t('Register New Account'), 'user/register', array('attributes' => array('class' => array('login-register button big'), 'title' => t('Create New User Account'))));
    if(theme_get_setting('defaulty_user_form_register_enable')) {
      $login_cta = l(t('Register New Account'), 'user/register', array('attributes' => array('class' => array('login-register button big'), 'title' => t('Create New User Account'))));
    }
    else {
      $login_cta = '';
    }
    
    
    
    // dpm($login_title);
    // dpm($login_copy);
    // dpm($login_cta);
  }
  
  // do the correct alter based on the current page.
  switch($page) {
    case 'register':
        _defaultu_user_form_left_side($vars['content']['system_main'], $register_title, $register_copy, $register_cta);
    break;
    case 'login':
      _defaultu_user_form_left_side($vars['content']['system_main'], $login_title, $login_copy, $login_cta);
    break;
    
  }
  
  if(TRUE) {
  }
  
  // dpm($path);
  dpm($page);
}

function defaulty_form_user_login_alter(&$form, &$form_state, $form_id) {
  $form['actions']['#suffix'] = l(t('Forgot Your Password?'), 'user/password', array('attributes' => array('class' => 'login-register button', 'title' => t('Create New User Account'))));

  // $form['name']['#prefix'] = '<section class="user-actions">';
  // $form['name']['#prefix'] .= l(t('Register New Account'), 'user/register', array('attributes' => array('class' => 'login-register button big', 'title' => t('Create New User Account'))));
  // $form['name']['#prefix'] .= '</section>';
}

function defaulty_form_user_register_form_alter($form, &$form_state, $form_id) {
  $form['account']['name']['#prefix'] = '<section class="user-actions">';
  $form['account']['name']['#prefix'] .= l(t('Sign Into Your Account!'), 'user/login', array('attributes' => array('class' => 'login-register button big', 'title' => t('Create New User Account'))));
  $form['account']['name']['#prefix'] .= '</section>';
}

function defaulty_form_alter(&$form, &$form_state, $form_id) {
  if(in_array('node_form', $form['#theme'])) {
    $keys = array('first' => '', 'last' => '', 'middle' => array());
    foreach($form['actions'] as $key => $action) {
      if($key != '#type' && $key != 'delete') {
        if($keys['first'] == '' || $keys['last'] == '') {
          $keys['first'] = $keys['last'] = $key;
        }

        if($action['#weight'] <= $form['actions'][$keys['first']]['#weight']) {
          $keys['first'] = $key;
        }
        elseif($action['#weight'] >= $form['actions'][$keys['last']]['#weight']) {
          $keys['last'] = $key;
        }
        else {
          $keys['middle'][] = $key;
        }
      }
    }

    $form['actions'][$keys['first']]['#attributes'] = array('class' => array('left'));
    $form['actions'][$keys['last']]['#attributes'] = array('class' => array('right'));

    foreach($keys['middle'] as $mid) {
      $form['actions'][$mid]['#attributes'] = array('class' => array('middle'));
    }
  }
}

function defaulty_textfield($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes($element, array('id', 'name', 'value', 'size', 'maxlength'));
  _form_set_class($element, array('form-text'));

  $extra = '';
  if ($element['#autocomplete_path'] && drupal_valid_path($element['#autocomplete_path'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#attributes']['id'] . '-autocomplete';
    $attributes['value'] = url($element['#autocomplete_path'], array('absolute' => TRUE));
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }

  $output = '<div class="textField-wrapper"><input' . drupal_attributes($element['#attributes']) . ' /></div>';

  return $output . $extra;
}

function defaulty_password($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'password';
  element_set_attributes($element, array('id', 'name', 'size', 'maxlength'));
  _form_set_class($element, array('form-text'));

  return '<div class="textField-wrapper passwordField-wrapper"><input' . drupal_attributes($element['#attributes']) . ' /></div>';
}

function defaulty_form_element_label($variables) {
  $element = $variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  // If title and required marker are both empty, output no label.
  if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
    return '';
  }

  // If the element is required, a required marker is appended to the label.
  $required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';

  $title = filter_xss_admin($element['#title']);

  $attributes = array();
  // Style the label as class option to display inline with the element.
  if ($element['#title_display'] == 'after') {
    $attributes['class'] = 'option';
  }
  // Show label only to screen readers to avoid disruption in visual flows.
  elseif ($element['#title_display'] == 'invisible') {
    $attributes['class'] = 'element-invisible';
  }

  if (!empty($element['#id'])) {
    $attributes['for'] = $element['#id'];
  }

  // The leading whitespace helps visually separate fields from inline labels.
  return ' <div class="form-label-wrapper"><label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label></div>\n";
}

function _defaultu_user_form_left_side(&$vars_content_system_main, $title, $body, $cta) {
  drupal_set_title($title);
  $vars_content_system_main['#prefix'] = '<section class="fifty left"><div class="inner">';
  $vars_content_system_main['#prefix'] .= $body;
  $vars_content_system_main['#prefix'] .= $cta;
  $vars_content_system_main['#prefix'] .= '</div></section>  <section class="fifty right">';
  $vars_content_system_main['#sufix'] = '</section>';
}